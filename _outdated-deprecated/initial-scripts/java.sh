#!/bin/bash
cd "$(dirname "$0")"

sudo aptitude install -y openjdk-11-jdk

if [ "$1" == "+sources" ]; then
  sudo aptitude install -y openjdk-11-source
fi
