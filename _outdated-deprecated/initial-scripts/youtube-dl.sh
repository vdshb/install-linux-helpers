#!/bin/bash
cd "$(dirname "$0")"

#sudo aptitude install -y youtube-dl # often leads to old version

#todo: Deprecated. Move to yt-dlp
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
