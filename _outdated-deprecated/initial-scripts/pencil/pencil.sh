#!/bin/bash
cd "$(dirname "$0")"

sudo aptitude install -y libgconf-2-4
sudo dpkg -i pencil_*_amd64.deb
sudo apt-get -f install -y

#fixme: pencil.deb doesn't work out of the box
#This hack makes pencil work:
#sudo chmod 4755 /opt/pencil*/chrome-sandbox
#But I don't like it as it's gain root privileges to pencil process
