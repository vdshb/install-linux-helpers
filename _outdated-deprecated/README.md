###list of scripts which are might be outdated (step 1 on the road to delete)

| script                  | comment                                                                                                         |
|-------------------------|-----------------------------------------------------------------------------------------------------------------|
| apps/net-tools.sh       | might be outdated                                                                                               |
| apps/dia.sh             | Pretty outdated. No opensource alternatives found yet. The team tries to radically update product, but not yet. |
| apps/texlive.sh         | seems not necessary as AsciiDoc showed up                                                                       |
| apps/texstudio.sh       | seems not necessary as AsciiDoc showed up                                                                       |
 

###list of deprecated scripts (step 2 on the road to delete)
| outdated document / script                              | comment                                       |
|---------------------------------------------------------|-----------------------------------------------|
| _outdated-deprecated/initial-scripts/java.sh            | replaced by apps/sdkman/sdkman-java.sh        |
| _outdated-deprecated/initial-scripts/pencil/pencil.sh   | some troubles with installation?              |
| _outdated-deprecated/initial-scripts/youtube-dl.sh      | move to yt-dlp                                | 
| ------------------------------------------------------- | --------------------------------------------- |
| _outdated-deprecated/manual-guides/Initial Setup.odt    | replaced with asciidoc. let's see how it work |    