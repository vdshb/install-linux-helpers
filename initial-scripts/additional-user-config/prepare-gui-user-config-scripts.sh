#!/bin/bash
cd "$(dirname "$0")"

username=$1
user_home_dir=$(../utils/__run-as-user.sh "$username" 'echo $HOME')

# Copy all folders structure to save relative paths between scripts
cp -r "../." "$user_home_dir/initial-config-scripts"
chown -R $username:$username "$user_home_dir/initial-config-scripts"
chmod -R u+x "$user_home_dir/initial-config-scripts"

{
  tee    "$user_home_dir/initial-config.sh" <<< '#!/bin/bash'
  tee -a "$user_home_dir/initial-config.sh" <<< 'cd "$(dirname "$0")"'
  tee -a "$user_home_dir/initial-config.sh" <<< ""
  tee -a "$user_home_dir/initial-config.sh" <<< './initial-config-scripts/additional-user-config/additional-gui-user-configuration.sh'
  tee -a "$user_home_dir/initial-config.sh" <<< ""
  tee -a "$user_home_dir/initial-config.sh" <<< "echo '=================================================================================='"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ''"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ' Configuration is finished.'"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ''"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ' [!!!] Re-login / Restart might be required for some changes to take an effect.'"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ''"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ' Feel free to remove:'"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ''"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo '   $user_home_dir/initial-config.sh'"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo '   $user_home_dir/initial-config-scripts'"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo ''"
  tee -a "$user_home_dir/initial-config.sh" <<< "echo '=================================================================================='"
  tee -a "$user_home_dir/initial-config.sh" <<< ""
} >> /dev/null

chown $username:$username "$user_home_dir/initial-config.sh"
chmod u+x "$user_home_dir/initial-config.sh"

