#!/bin/bash
cd "$(dirname "$0")"

##
# This script is supposed to be run under GUI environment after single [konsole] run
##

source ../utils/__user-dialogs.sh

../apps/yakuake/yakuake-config-local.sh

_ask_default_no "Install telegram"
answer=$?
if [ "$answer" = "1" ]; then
    ../apps/telegram/telegram-local.sh
fi

_ask_default_no "Install Jetbrains toolbox"
answer=$?
if [ "$answer" = "1" ]; then
    ../apps/jetbrains/toolbox-local.sh
fi
