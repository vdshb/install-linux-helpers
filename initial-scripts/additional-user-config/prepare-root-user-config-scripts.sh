#!/bin/bash
cd "$(dirname "$0")"

username_owns_fun_files=$1
source ../apps/shared-data/settings/shared-group-name.sh
root_home_dir=$(su - -c "cd ~/; pwd")

# prepare mount fun disks script
cp ../apps/shared-data/mount-fun-drives.sh "$root_home_dir"
chmod u+x "$root_home_dir/mount-fun-drives.sh"

# prepare "update permissions on fun disks" script
{
  tee    "$root_home_dir/reset-fun-files-permissions.sh" <<< '#!/bin/bash'
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< 'cd "$(dirname "$0")"'
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< ""
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< "# Check if you really need this actions:"
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< "chown -R \"$username_owns_fun_files:$shared_fun_group_name\" /mnt/hdd-14T-ph_Mod"
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< "chown -R \"$username_owns_fun_files:$shared_fun_group_name\" /mnt/hdd-14T-ph_Diff"
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< "chmod -R g+rw /mnt/hdd-14T-ph_Mod"
  tee -a "$root_home_dir/reset-fun-files-permissions.sh" <<< "chmod -R g+rw /mnt/hdd-14T-ph_Diff"
} >> /dev/null

chown root:root "$root_home_dir/reset-fun-files-permissions.sh"
chmod u+x "$root_home_dir/reset-fun-files-permissions.sh"

#================= prepare "initial fun disks setup" script =================

# Copy all folders structure to save relative paths between scripts
cp -r "../." "$root_home_dir/initial-config-scripts"
chmod -R u+x "$root_home_dir/initial-config-scripts"

{
  tee    "$root_home_dir/initial-fun-drives-setup.sh" <<< '#!/bin/bash'
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< 'cd "$(dirname "$0")"'
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '====================== current disks situation ========================'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "fstab -l"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '======================================================================='"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< ""
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '======================================================================='"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!] Default fun mounting is (Details: $root_home_dir/mount-fun-drives.sh):'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!]'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!] /dev/sdb1 -> /mnt/hdd-14T-ph_Mod'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!] /dev/sdc1 -> /mnt/hdd-14T-ph_Diff'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!]'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!] Do not forget to check whether'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!]   - /dev/sdb1'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!]   - /dev/sdc1'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '[!!!] are present.'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "echo '======================================================================='"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< ""
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "source ./initial-config-scripts/utils/__user-dialogs.sh"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< ""
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "_ask_default_no 'Continue'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< 'answer=$?'
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< 'if [ "$answer" = "1" ]; then'
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./mkdir /mnt/hdd-14T-ph_Mod"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./mkdir /mnt/hdd-14T-ph_Diff"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./mount-fun-drives.sh"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./initial-config-scripts/apps/shared-data/util/set-group-permissions.sh /mnt/hdd-14T-ph_Mod  $shared_fun_group_name"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./initial-config-scripts/apps/shared-data/util/set-group-permissions.sh /mnt/hdd-14T-ph_Diff $shared_fun_group_name"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    ./reset-fun-files-permissions.sh"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< ""
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '======================================================================='"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '   Fun drives initial configuration is finished.'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '   Feel free to remove:'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo ''"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '      - $root_home_dir/initial-config-scripts'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '      - $root_home_dir/initial-fun-drives-setup.sh'"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "    echo '======================================================================='"
  tee -a "$root_home_dir/initial-fun-drives-setup.sh" <<< "fi"
} >> /dev/null

chown root:root "$root_home_dir/initial-fun-drives-setup.sh"
chmod u+x "$root_home_dir/initial-fun-drives-setup.sh"

# exit
echo "================================================================================"
echo ""
echo " Root scripts are created. Feel free to use them and delete after initial configuration:"
echo ""
echo "   $root_home_dir/initial-fun-drives-setup.sh"
echo "   $root_home_dir/initial-config-scripts"
echo ""
echo " Also keep for future use:"
echo ""
echo "   $root_home_dir/mount-fun-drives.sh"
echo "   $root_home_dir/reset-fun-files-permissions.sh"
echo ""
echo "================================================================================"
