#!/bin/bash
cd "$(dirname "$0")"

mode=$1

source ./utils/__user-dialogs.sh

function make_sure_run_is_prepared() {
    environment=$1
    _ask_default_no "Are you sure want to install $environment environment"
    return $?
}

if [ "$mode" != "--home" ] && [ "$mode" != "--work" ] && [ "$mode" != "--server" ] && [ "$mode" != "--help" ]; then
    echo "Exception. Correct option required. "
    echo ""
    mode="--help"
fi
if [ "$mode" = "--home" ]; then
    echo "======================================================================="
    echo "[!!!] Don't forget to download all binaries to install:"
    echo "[!!!]    - chrome"
    echo "[!!!]    - opera"
    echo "[!!!]    - vivaldi"
    echo "[!!!]    - skype"
    echo "[!!!]    - slack"
    echo "[!!!]    - telegram"
    echo "[!!!]    - jetbrains-toolbox"
    echo "======================================================================="
    echo ""
    echo "======================================================================="
    echo "    Don't forget to run as non-root user who will be reconfigured."
    echo "======================================================================="
    echo ""
    make_sure_run_is_prepared $mode
    answer=$?
    if [ "$answer" = "1" ]; then
        echo "======================================================================="
        echo "           Be ready to enter root password to proceed."
        echo "======================================================================="
        echo ""
        utils/__run-as-root.sh "./__configure-home-environment.sh $USER"
    fi
fi
if [ "$mode" = "--work" ]; then
    echo "======================================================================="
    echo "[!!!] Don't forget to download all binaries to install:"
    echo "[!!!]    - chrome"
    echo "[!!!]    - opera"
    echo "[!!!]    - vivaldi"
    echo "[!!!]    - skype"
    echo "[!!!]    - slack"
    echo "[!!!]    - telegram"
    echo "[!!!]    - jetbrains-toolbox"
    echo "======================================================================="
    echo ""
    echo "======================================================================="
    echo "    Don't forget to run as non-root user who will be reconfigured."
    echo "======================================================================="
    echo ""
    make_sure_run_is_prepared $mode
    answer=$?
    if [ "$answer" = "1" ]; then
        echo "======================================================================="
        echo "           Be ready to enter root password to proceed."
        echo "======================================================================="
        echo ""
        utils/__run-as-root.sh "./__configure-work-environment.sh $USER"
    fi
fi
if [ "$mode" = "--server" ]; then
    echo "======================================================================="
    echo "    Don't forget to run as non-root user who will be reconfigured."
    echo "======================================================================="
    echo ""
    make_sure_run_is_prepared $mode
    answer=$?
    if [ "$answer" = "1" ]; then
        echo "======================================================================="
        echo "           Be ready to enter root password to proceed."
        echo "======================================================================="
        echo ""
        utils/__run-as-root.sh "./__configure-server-environment.sh $USER"
    fi
fi
if [ "$mode" = "--help" ]; then
    echo "Script installs initial environment for new debian OS. Usage:"
    echo ""
    echo " install.sh [option]"
    echo ""
    echo "[option] might be:"
    echo " --home   - install home environment"
    echo " --server - install server environment"
    echo " --work   - install work environment"
    echo " --help   - show this message"
    echo ""
fi
