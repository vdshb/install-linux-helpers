#!/bin/bash
cd "$(dirname "$0")"

current_non_root_user_username=$1

if [ "$current_non_root_user_username" == "" ]; then
    echo '================================================================================='
    echo 'This is an inner script. Please use "install.sh" for system installation'
    echo '================================================================================='
    exit 0
fi

# prepare
utils/__prepare-scripts-privileges.sh

#base
apps/locales-en.sh
apps/timezone-utc.sh
apps/aptitude.sh
apps/sudo-install.sh
#apps/grub-config.sh # Uncomment for personal VM

#utils
apps/nano.sh
apps/hexedit.sh
apps/curl.sh
apps/mc.sh
apps/gnupg.sh
apps/docker/docker.sh
apps/git.sh
apps/sysstat.sh
apps/psmisc.sh
apps/expect.sh
apps/zip.sh

#user configuration
__configure-server-user.sh "$current_non_root_user_username"
