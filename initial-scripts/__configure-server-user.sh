#!/bin/bash
cd "$(dirname "$0")"

username=$1

if [ "$username" == "" ]; then
    echo '================================================================================='
    echo 'This is an inner script. Please use "install.sh" for system installation'
    echo '================================================================================='
    exit 0
fi

user_home_dir=$(utils/__run-as-user.sh "$username" 'echo $HOME')

source ./utils/__user-dialogs.sh

# prepare
utils/__prepare-scripts-privileges.sh

# general
utils/__run-as-user.sh "$username" "apps/ssh-keys-local.sh"
utils/__run-as-user.sh "$username" "apps/bash-config-local.sh"

# add sudo privileges
_ask_default_no "Give sudo privilege to $username"
answer=$?
if [ "$answer" = "1" ]; then
    apps/add-user-sudo-privileges.sh $username
fi

_ask_default_no "Grant $username docker access"
answer=$?
if [ "$answer" = "1" ]; then
    apps/docker/docker-user-config.sh "$username"
fi

#java-tools install
_ask_default_no "Install sdkman to $username"
answer=$?
if [ "$answer" = "1" ]; then
    utils/__run-as-user.sh "$username" "apps/sdkman/-sdkman-local.sh"

    _ask_default_no "Install java by sdkman to $username"
    answer=$?
    if [ "$answer" = "1" ]; then
        utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-java-local.sh"
    fi
    _ask_default_no "Install kotlin by sdkman to $username"
    answer=$?
    if [ "$answer" = "1" ]; then
        utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-kotlin-local.sh"
    fi
    _ask_default_no "Install gradle by sdkman to $username"
    answer=$?
    if [ "$answer" = "1" ]; then
        utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-gradle-local.sh"
    fi
    _ask_default_no "Install maven by sdkman to $username"
    answer=$?
    if [ "$answer" = "1" ]; then
        utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-maven-local.sh"
    fi
fi
