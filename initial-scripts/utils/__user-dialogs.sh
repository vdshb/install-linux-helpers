#!/bin/bash


function _ask_default_no() {
    question=$1
    read -p "$question [y/N]? " -n 1 -r
    echo
    if [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ]; then
        return 1
    fi
    return 0
}

function _ask_default_yes() {
    question=$1
    read -p "$question [Y/n]? " -n 1 -r
    echo
    if [ "$REPLY" = "N" ] || [ "$REPLY" = "n" ]; then
        return 0
    fi
    return 1
}

export -f _ask_default_no
export -f _ask_default_yes
