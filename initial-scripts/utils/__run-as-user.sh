#!/bin/bash

user=$1
command=$2
dir=$(pwd)

su - "$user" -c "cd '$dir'; $command"
