#!/bin/bash
cd "$(dirname "$0")"

find ../ -name "*.sh"       -exec chmod u+rx {} \;
find ../ -name "*-local.sh" -exec chmod a+rx {} \;