#!/bin/bash
cd "$(dirname "$0")"

username=$1

#copy config
../../utils/__run-as-user.sh "$username" "mkdir -p '~/.config/vlc'"

chmod a+r vlcrc
../../utils/__run-as-user.sh "$username" "cp vlcrc '~/.config/vlc'"
../../utils/__run-as-user.sh "$username" "chmod a-r '~/.config/vlc/vlcrc'"
../../utils/__run-as-user.sh "$username" "chmod u+rw '~/.config/vlc/vlcrc'"