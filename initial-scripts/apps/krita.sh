#!/bin/bash
cd "$(dirname "$0")"

##
## Image editor (closer to paint than gimp).
##
aptitude install -y krita
