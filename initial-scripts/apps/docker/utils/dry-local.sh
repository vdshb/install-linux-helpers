#!/bin/bash
cd "$(dirname "$0")"

mkdir -p ~/.local/bin
curl -sL https://github.com/moncho/dry/releases/download/v0.11.1/dry-linux-amd64 -o ~/.local/bin/dry
chmod u+x ~/.local/bin/dry