#!/bin/bash
cd "$(dirname "$0")"

mkdir -p ~/.local/bin
mkdir -p /tmp/lazydocker/unzip
curl -sL https://github.com/jesseduffield/lazydocker/releases/download/v0.12/lazydocker_0.12_Linux_x86_64.tar.gz -o /tmp/lazydocker/ld.tar.gz
tar -xf /tmp/lazydocker/ld.tar.gz -C /tmp/lazydocker/unzip
mv /tmp/lazydocker/unzip/lazydocker ~/.local/bin
chmod u+x ~/.local/bin/lazydocker
rm -rf /tmp/lazydocker