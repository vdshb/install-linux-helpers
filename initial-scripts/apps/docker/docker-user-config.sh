#!/bin/bash
cd "$(dirname "$0")"

username=$1

usermod -aG docker "$username"
