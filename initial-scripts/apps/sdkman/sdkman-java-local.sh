#!/bin/bash
cd "$(dirname "$0")"

source "$HOME/.sdkman/bin/sdkman-init.sh"
LATEST_BELLSOFT_JAVA_VERSION=$(sdk list java | grep librca | head -n 1 | cut -d'|' -f6)

# Alternative (See: ~/.sdkman/src/sdkman-list.sh::__sdkman_list_versions) :
#list=$(curl -s "${SDKMAN_CANDIDATES_API}/candidates/java/${SDKMAN_PLATFORM}/versions/list?installed=")
#LATEST_BELLSOFT_JAVA_VERSION=$(cat <<< $list | grep librca | head -n 1 | cut -d'|' -f6)

sdk install java $LATEST_BELLSOFT_JAVA_VERSION

#cat <<< $list                              #debug info
#echo ""                                    #debug info
#echo "******************************"      #debug info
#echo ""                                    #debug info
#cat <<< LATEST_BELLSOFT_JAVA_VERSION       #debug info
