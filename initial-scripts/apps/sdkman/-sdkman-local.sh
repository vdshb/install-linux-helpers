#!/bin/bash
cd "$(dirname "$0")"

# curl & zip must be already installed
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
