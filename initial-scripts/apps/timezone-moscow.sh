#!/bin/bash
cd "$(dirname "$0")"

echo "Europe/Moscow" | tee /etc/timezone >> /dev/null
ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
