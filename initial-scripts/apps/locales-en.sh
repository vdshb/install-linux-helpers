#!/bin/bash
cd "$(dirname "$0")"

apt-get install -y locales
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
echo 'LANG="en_US.UTF-8"' | tee /etc/default/locale >> /dev/null
dpkg-reconfigure --frontend=noninteractive locales
update-locale LANG=en_US.UTF-8