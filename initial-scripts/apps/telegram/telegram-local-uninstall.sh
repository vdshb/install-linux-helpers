#!/bin/bash
cd "$(dirname "$0")"

rm -rf ~/.local/share/TelegramDesktop
rm  -f ~/.local/share/applications/*Telegram?Desktop*
rm  -f ~/.local/share/icons/telegram.png
rm -rf ~/.local/lib/telegram