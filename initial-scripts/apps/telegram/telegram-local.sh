#!/bin/bash
cd "$(dirname "$0")"

mkdir -p ~/.local/lib/telegram/unzip
tar -xf tsetup.*.tar.xz -C ~/.local/lib/telegram/unzip
mv ~/.local/lib/telegram/unzip/Telegram/* ~/.local/lib/telegram/
rm -rf ~/.local/lib/telegram/unzip

~/.local/lib/telegram/Telegram &
LAST_PID=$!
echo "==========================================================="
echo "    Telegram is just started as an installation step."
echo "             It will be killed in 5 sec..."
echo "==========================================================="
sleep 5
kill $LAST_PID
