#!/bin/bash
cd "$(dirname "$0")"

# The sysstat package contains the following system performance tools:
#
#  - sar: collects and reports system activity information;
#  - iostat: reports CPU utilization and disk I/O statistics;
#  - tapestat: reports statistics for tapes connected to the system;
#  - mpstat: reports global and per-processor statistics;
#  - pidstat: reports statistics for Linux tasks (processes);
#  - sadf: displays data collected by sar in various formats;
#  - cifsiostat: reports I/O statistics for CIFS filesystems.
#
# The statistics reported by sar deal with I/O transfer rates, paging activity, process-related activities, interrupts, network activity,
# memory and swap space utilization, CPU utilization, kernel activities and TTY statistics, among others.
# Both UP and SMP machines are fully supported.
aptitude install -y sysstat
