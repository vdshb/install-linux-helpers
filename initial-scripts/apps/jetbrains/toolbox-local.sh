#!/bin/bash
cd "$(dirname "$0")"

mkdir -p ~/jetbrains-toolbox-unzip
tar -xf jetbrains-toolbox*.tar.gz -C ~/jetbrains-toolbox-unzip

echo "======================================================================"
echo "    Jetbrains Toolbox is starting as an installation step."
echo "    It supposed to finish installation process in ~10 sec..."
echo ""
echo "    Apps configuration downloading also takes some time."
echo "    Don't worry first 1-2 minute after installation toolbox is empty."
echo "======================================================================"
~/jetbrains-toolbox-unzip/jetbrains-toolbox*/jetbrains-toolbox

rm -rf ~/jetbrains-toolbox-unzip
