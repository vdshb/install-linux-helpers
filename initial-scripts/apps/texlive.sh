#!/bin/bash
cd "$(dirname "$0")"

##
#  LaTeX compiler (With some additional plugins. Not all of them. Think about installing [texlive-full] package)
##
aptitude install -y texlive texlive-lang-cyrillic texlive-formats-extra
