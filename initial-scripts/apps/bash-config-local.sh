#!/bin/bash
cd "$(dirname "$0")"

{
  tee -a ~/.bashrc <<< ""
  tee -a ~/.bashrc <<< "#[vdshb] add locally executable path"
  tee -a ~/.bashrc <<< "PATH=$HOME/.local/bin:$PATH"
} >> /dev/null