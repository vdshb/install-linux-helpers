#!/bin/bash
cd "$(dirname "$0")"

aptitude -y install qemu-kvm libvirt-bin bridge-utils

#GUI
aptitude -y install virt-manager
