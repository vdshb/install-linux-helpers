#!/bin/bash
cd "$(dirname "$0")"

# See: https://wiki.debian.org/VirtualBox
# fasttrack (https://fasttrack.debian.net/) repositories must be configured during aptitude.sh

aptitude install -y virtualbox #todo: Check on real hardware, not VM. Potentially I might also need packages: virtualbox-dkms linux-headers-amd64
aptitude install -y virtualbox-ext-pack
