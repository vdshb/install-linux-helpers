#!/bin/bash
cd "$(dirname "$0")"

aptitude install -y libreoffice libreoffice-kde5

# From backports (not tested yet):
#aptitude -t bullseye-backports install -y libreoffice
#aptitude install -y libreoffice-kde5
