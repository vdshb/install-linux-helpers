#!/bin/bash
cd "$(dirname "$0")"

{
  echo $'\n#Max file watchers number' | tee -a /etc/sysctl.conf
  echo 'fs.inotify.max_user_watches = 2000000' | tee -a /etc/sysctl.conf
} >> /dev/null
sysctl -p --system