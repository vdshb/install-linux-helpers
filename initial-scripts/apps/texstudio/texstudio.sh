#!/bin/bash
cd "$(dirname "$0")"

# download AppImage from https://www.fosshub.com/qBittorrent.html or https://www.qbittorrent.org/download.php

#todo: simplify installing AppImage. See:
#        https://askubuntu.com/a/1125304
#        https://askubuntu.com/a/935249

current_dir=$(pwd)

cp texstudio-*.AppImage /usr/bin/texstudio.AppImage
chown root:root /usr/bin/texstudio.AppImage
chmod a-w /usr/bin/texstudio.AppImage
chmod u+w /usr/bin/texstudio.AppImage
chmod a+rx /usr/bin/texstudio.AppImage


rm -rf /tmp/tex-studio-app-extract
mkdir /tmp/tex-studio-app-extract
cd /tmp/tex-studio-app-extract
/usr/bin/texstudio.AppImage --appimage-extract # extracts to <current-dir>/squashfs-root
cp /tmp/tex-studio-app-extract/squashfs-root/*.desktop  /usr/share/applications/texstudio.desktop
sed -i -e 's/Exec=texstudio /Exec=texstudio.AppImage /' /usr/share/applications/texstudio.desktop
cp /tmp/tex-studio-app-extract/squashfs-root/*.svg  /usr/share/icons/texstudio.svg

cd "$current_dir"
rm -rf /tmp/tex-studio-app-extract
