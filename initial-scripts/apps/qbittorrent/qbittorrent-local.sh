#!/bin/bash
cd "$(dirname "$0")"

# download AppImage from https://www.fosshub.com/qBittorrent.html or https://www.qbittorrent.org/download.php

#todo: simplify installing AppImage. See:
#        https://askubuntu.com/a/1125304
#        https://askubuntu.com/a/935249

current_dir=$(pwd)

mkdir -p ~/.local/bin
cp qbittorrent*.AppImage ~/.local/bin/qbittorrent.AppImage
chmod u+x ~/.local/bin/qbittorrent.AppImage


rm -rf /tmp/qbittorrent-app-extract
mkdir /tmp/qbittorrent-app-extract
mkdir -p ~/.local/share/applications
mkdir -p ~/.local/share/icons

cd /tmp/qbittorrent-app-extract
~/.local/bin/qbittorrent.AppImage --appimage-extract # extracts to <current-dir>/squashfs-root
cp /tmp/qbittorrent-app-extract/squashfs-root/*.desktop  ~/.local/share/applications/qbittorrent.desktop
sed -i -e 's/Exec=qbittorrent /Exec=qbittorrent.AppImage /' ~/.local/share/applications/qbittorrent.desktop
cp /tmp/qbittorrent-app-extract/squashfs-root/*.svg  ~/.local/share/icons/qbittorrent.svg


cd "$current_dir"
rm -rf /tmp/qbittorrent-app-extract
