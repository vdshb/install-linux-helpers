#!/bin/bash
cd "$(dirname "$0")"

username=$1

echo "$username   ALL=(ALL:ALL) ALL" >> /etc/sudoers
