#!/bin/bash
cd "$(dirname "$0")"

sed -i -e 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub
update-grub