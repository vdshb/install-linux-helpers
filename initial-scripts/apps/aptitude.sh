#!/bin/bash
cd "$(dirname "$0")"

apt-get install -y aptitude

#basic linux config
dpkg --add-architecture i386
aptitude -y install apt-transport-https

#add backports
{
  echo $'\n#backports' | tee -a /etc/apt/sources.list
  echo "deb http://deb.debian.org/debian bullseye-backports main" | tee -a /etc/apt/sources.list
  echo "deb-src http://deb.debian.org/debian bullseye-backports main" | tee -a /etc/apt/sources.list
} >> /dev/null

#add fasttrack (https://fasttrack.debian.net/)
aptitude install -y fasttrack-archive-keyring
{
  echo $'\n#fasttrack' | tee -a /etc/apt/sources.list
  echo "deb https://fasttrack.debian.net/debian-fasttrack/ bullseye-fasttrack main contrib" | tee -a /etc/apt/sources.list
  echo "deb https://fasttrack.debian.net/debian-fasttrack/ bullseye-backports-staging main contrib" | tee -a /etc/apt/sources.list
} >> /dev/null

aptitude update
