#!/bin/bash
cd "$(dirname "$0")"

# contains utils:
# - killall
# - pstree
# - prtstat
# - fuser
# - peekfd
aptitude install -y psmisc
