#!/bin/bash
cd "$(dirname "$0")"

username=$1

#copy config
../../utils/__run-as-user.sh "$username" "mkdir -p '~/.config/smplayer'"

chmod a+r smplayer.ini
../../utils/__run-as-user.sh "$username" "cp smplayer.ini '~/.config/smplayer'"
../../utils/__run-as-user.sh "$username" "chmod a-r '~/.config/smplayer/smplayer.ini'"
../../utils/__run-as-user.sh "$username" "chmod u+rw '~/.config/smplayer/smplayer.ini'"