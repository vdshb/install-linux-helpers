#!/bin/bash
cd "$(dirname "$0")"

#fixme: Make work on debian-11. Some dependency problems with the last version slack-4.24.0 on debian-11 (can't find libappindicator3-1)
        See: https://www.reddit.com/r/debian/comments/pn1oia/what_happened_to_libappindicator31_in_debian_11/

dpkg -i slack-desktop-*-amd64.deb
apt-get -f install -y
