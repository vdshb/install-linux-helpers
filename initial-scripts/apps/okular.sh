#!/bin/bash
cd "$(dirname "$0")"

aptitude install -y okular
aptitude install -y okular-extra-backends
