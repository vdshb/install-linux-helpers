#!/bin/bash
cd "$(dirname "$0")"

aptitude install -y task-print-server

if [ "$1" == "--hp" ]; then
    aptitude install -y hplip
    aptitude install -y gtk2-engines-pixbuf libtool-bin python3-dbus.mainloop.qt cups-bsd python3-dev \
                             libjpeg62-turbo-dev cups-client libcupsimage2 libcups2-dev libsane-dev avahi-utils xsane snmp libcupsimage2-dev \
                             libsnmp-dev libdbus-1-dev libcups2 libtool cups python3-notify2 python3-pyqt4 libusb-1.0-0-dev
    echo
    echo "**************************************************"
    echo "*           HP-DOCTOR printer set-up             *"
    echo "**************************************************"
    hp-doctor
#    hp-doctor installs some plugin for printer to work. (So not recommended way of printer connection in CUPS admin-menu)
fi
