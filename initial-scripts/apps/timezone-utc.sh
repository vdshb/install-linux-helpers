#!/bin/bash
cd "$(dirname "$0")"

echo "UTC"  | tee /etc/timezone >> /dev/null
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
