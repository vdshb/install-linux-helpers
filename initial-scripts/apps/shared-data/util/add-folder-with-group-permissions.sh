#!/bin/bash
cd "$(dirname "$0")"

folder_path=$1
group_name=$2
watcher_autostart_id=$3

# ======================== Create folder and set ACL permissions ========================

mkdir $folder_path
./set-group-permissions.sh "$folder_path" "$group_name"

# ============ Add watcher to monitor moved files and change group permissions ============

watch_util_dir="/root/.local/lib/file_watcher"                                   #Must not contain spaces
watch_util_path="$watch_util_dir/watch_dir_and_set_group_owner_for_new_files.sh" #Must not contain spaces
watch_service_name=watch_dir_and_set_group_owner_for_new_files_${watcher_autostart_id}.service
watch_service_path=/etc/systemd/system/$watch_service_name

mkdir -p "$watch_util_dir"
cp ../demon/watch_dir_and_set_group_owner_for_new_files.sh "$watch_util_path"
chown root:root "$watch_util_path"
chmod u+x "$watch_util_path"

{
  tee    "$watch_service_path" <<< "[Unit]"
  tee -a "$watch_service_path" <<< "Description=Sets group to all files moved into $folder_path"
  #tee -a "$watch_service_path" <<< "After=-.mount"
  tee -a "$watch_service_path" <<< ""
  tee -a "$watch_service_path" <<< "[Service]"
  tee -a "$watch_service_path" <<< "ExecStart=$watch_util_path \"$folder_path\" \"$group_name\"" #That's why [watch_util_path] must not contain spaces
  tee -a "$watch_service_path" <<< "User=root"
  tee -a "$watch_service_path" <<< "Group=root"
  tee -a "$watch_service_path" <<< "Restart=always"
  tee -a "$watch_service_path" <<< ""
  tee -a "$watch_service_path" <<< "[Install]"
  tee -a "$watch_service_path" <<< "WantedBy=multi-user.target"
} >> /dev/null
systemctl daemon-reload
systemctl enable $watch_service_name
systemctl start $watch_service_name
