#!/bin/bash
cd "$(dirname "$0")"

folder_path=$1
group_name=$2

chown root:"$group_name" "$folder_path"
chgrp -R "$group_name" "$folder_path"
chmod -R g+s "$folder_path"                         # group inheritance
setfacl -R -m "g:$group_name:rwX" "$folder_path"    # permissions for directory itself
setfacl -R -d -m "g:$group_name:rwX" "$folder_path" # default permissions for newly created files
setfacl -R -d -m "o::---" "$folder_path"            # drop all access to other users
