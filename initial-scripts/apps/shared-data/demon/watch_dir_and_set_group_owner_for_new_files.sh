#!/bin/bash
cd "$(dirname "$0")"

watch_path=$1
group_owner=$2

#todo: think about moving to fanotify (it uses much less file descriptors == RAM)
inotifywait -r -m $watch_path -e moved_to |
    while read dir action file; do
        #echo "The file '$file' appeared in directory '$dir' via '$action'"
        chgrp -R $group_owner "$dir$file"
        chmod -R g+rw "$dir$file"
    done


