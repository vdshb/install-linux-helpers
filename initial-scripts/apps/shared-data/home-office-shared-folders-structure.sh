#!/bin/bash
cd "$(dirname "$0")"

# ========================= prepare shared groups =========================
source ./settings/shared-group-name.sh

groupadd $shared_group_name
groupadd $shared_fun_group_name

# ============== create shared folders (ACL-group permissions) ==============

util/add-folder-with-group-permissions.sh /_data $shared_group_name data

