#!/bin/bash
cd "$(dirname "$0")"

# mount disks script
# it supposed to be run with root privileges

ph_Mod_id=/dev/sdb1
ph_Diff_id=/dev/sdc1

echo "mounting $ph_Mod_id -> /mnt/hdd-14T-ph_Mod"
echo "mounting $ph_Diff_id -> /mnt/hdd-14T-ph_Diff"
echo "====================== fdisk -l [for debug reasons] ====================="
/sbin/fdisk -l
echo "========================================================================="

function decrypt_and_mount() {
    local device_id=$1
    local mapper_id=$2
    local mount_point=$3

    /sbin/cryptsetup luksOpen "$device_id" "$mapper_id"
    mount "/dev/mapper/$mapper_id" "$mount_point"
}

decrypt_and_mount "$ph_Mod_id"  PH_1 /mnt/hdd-14T-ph_Mod
decrypt_and_mount "$ph_Diff_id" PH_2 /mnt/hdd-14T-ph_Diff
