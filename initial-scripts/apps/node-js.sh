#!/bin/bash
cd "$(dirname "$0")"

installation_type=$1
installation_version=$2

if [ $installation_type == "--straight-global" ]; then
    if [ $installation_version == "--lts" ]; then
        curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
    fi
    if [ $installation_version == "--latest" ]; then
        curl -fsSL https://deb.nodesource.com/setup_17.x | bash -
    fi
    aptitude install -y nodejs
    aptitude install -y build-essential
fi

if [ $installation_type == "--volta-local" ]; then
    curl https://get.volta.sh | bash
    export VOLTA_HOME="$HOME/.volta"
    export PATH="$VOLTA_HOME/bin:$PATH"
    if [ $installation_version == "--lts" ]; then
        volta install node
    fi
    if [ $installation_version == "--latest" ]; then
        volta install node@17
    fi
fi
