#!/bin/bash
cd "$(dirname "$0")"

##
# Set of tools to watch files changes. Includes:
# - inotifywait
# - inotifywatch
##
aptitude install -y inotify-tools
