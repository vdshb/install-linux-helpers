#!/bin/bash
cd "$(dirname "$0")"

## Konsole profile
cp -r konsole  ~/.local/share/
## Konsole config
cp konsolerc ~/.config/
## Yakuake config
cp yakuakerc ~/.config/

# start yakuake once
yakuake &
LAST_PID=$!
echo "==========================================================="
echo "    Yakuake is just started as an installation step."
echo "             It will be killed in 5 sec..."
echo "==========================================================="
sleep 5
kill $LAST_PID

# Set Meta+Z shortcut to call yakuake
sed -i -e 's/toggle-window-state=F12,F12/toggle-window-state=Meta+Z,F12/' ~/.config/kglobalshortcutsrc

# Add to autostart
mkdir -p ~/.config/autostart/
ln -s /usr/share/applications/org.kde.yakuake.desktop ~/.config/autostart/org.kde.yakuake.desktop
