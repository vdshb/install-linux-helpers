[Appearance]
AntiAliasFonts=true
BoldIntense=false
ColorScheme=Linux
Font=Hack,10.5,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[General]
Name=Contrast green
Parent=FALLBACK/
TerminalColumns=120
TerminalRows=40

[Interaction Options]
CopyTextAsHTML=false
