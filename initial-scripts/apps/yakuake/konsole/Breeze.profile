[Appearance]
AntiAliasFonts=true
BoldIntense=false
ColorScheme=Breeze
Font=Hack,10.5,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[General]
Name=Breeze
Parent=FALLBACK/
TerminalColumns=120
TerminalRows=40

[Interaction Options]
CopyTextAsHTML=false
