#!/bin/bash
cd "$(dirname "$0")"

current_non_root_user_username=$1

if [ "$current_non_root_user_username" == "" ]; then
    echo '================================================================================='
    echo 'This is an inner script. Please use "install.sh" for system installation'
    echo '================================================================================='
    exit 0
fi

# prepare
utils/__prepare-scripts-privileges.sh

#base
apps/locales-en-with-ru.sh
apps/timezone-moscow.sh
apps/aptitude.sh
apps/grub-config.sh
apps/file-watch-config.sh

#utils
apps/psmisc.sh
apps/nano.sh
apps/hexedit.sh
apps/curl.sh
apps/mc.sh
apps/git.sh
apps/gnupg.sh
apps/sysstat.sh
#apps/openvpn.sh
#apps/network-manager-openvpn.sh
apps/fonts_droid.sh
apps/docker/docker.sh
apps/yakuake/yakuake.sh
apps/partitionmanager.sh
#apps/transmission.sh           #todo: leave just for VPN-free environments
#apps/ktorrent.sh               #todo: leave just for VPN-free environments
apps/yt-dlp.sh
apps/kalarm.sh
apps/wine.sh
apps/expect.sh
apps/lm-sensors.sh
apps/zip.sh
apps/tree.sh
apps/rename.sh
apps/inotify-tools.sh
apps/cryptsetup.sh
apps/rsync.sh
apps/desktop-file-utils.sh

#communication-tools
apps/chrome/chrome.sh
apps/firefox.sh
apps/vivaldi/vivaldi.sh
apps/opera/opera.sh
apps/skype/skype.sh
#apps/slack/slack.sh            #fixme: debian-11 installation problems

#dev-tools
apps/libreoffice.sh
apps/dia.sh
apps/postgresql-client.sh
apps/virtual-box/virtual-box.sh
apps/kvm/kvm.sh
#apps/texlive.sh
#apps/texstudio/texstudio.sh

#multimedia
apps/smplayer/smplayer.sh
apps/vlc/vlc.sh
apps/okular.sh
apps/krita.sh
apps/kazam.sh
apps/inkscape.sh
apps/kolourpaint.sh

#devices
#apps/printer.sh --hp  #printer must be connected

#users
echo "======================================================================"
echo "                 Configuring current user '$current_non_root_user_username'."
echo "======================================================================"
./__configure-work-user.sh "$current_non_root_user_username"
