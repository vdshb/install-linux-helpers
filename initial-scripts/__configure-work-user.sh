#!/bin/bash
cd "$(dirname "$0")"

username=$1

if [ "$username" == "" ]; then
    echo '================================================================================='
    echo 'This is an inner script. Please use "install.sh" for system installation'
    echo '================================================================================='
    exit 0
fi

user_home_dir=$(utils/__run-as-user.sh "$username" 'echo $HOME')

source ./utils/__user-dialogs.sh

# prepare
utils/__prepare-scripts-privileges.sh

# general
utils/__run-as-user.sh "$username" "apps/ssh-keys-local.sh"
utils/__run-as-user.sh "$username" "apps/bash-config-local.sh"
additional-user-config/prepare-gui-user-config-scripts.sh "$username"
apps/virtual-box/virtual-box-user-config.sh "$username"
apps/kvm/kvm-user-config.sh "$username"
apps/smplayer/smplayer-config.sh "$username"
apps/vlc/vlc-config.sh "$username"

#java-tools install
utils/__run-as-user.sh "$username" "apps/sdkman/-sdkman-local.sh"
utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-java-local.sh"
utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-kotlin-local.sh"
utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-gradle-local.sh"
utils/__run-as-user.sh "$username" "apps/sdkman/sdkman-maven-local.sh"
utils/__run-as-user.sh "$username" "apps/node-js.sh --volta-local --lts"

# add sudo privileges
_ask_default_no "Give sudo privilege to $username"
answer=$?
if [ "$answer" = "1" ]; then
    apps/add-user-sudo-privileges.sh $username
fi

_ask_default_no "Install qBittorrent for user $username"
answer=$?
if [ "$answer" = "1" ]; then
    utils/__run-as-user.sh "$username" "apps/qbittorrent/qbittorrent-local.sh"
fi

_ask_default_no "Grant $username docker access"
answer=$?
if [ "$answer" = "1" ]; then
    apps/docker/docker-user-config.sh "$username"
    utils/__run-as-user.sh "$username" "apps/docker/utils/dry-local.sh"
    utils/__run-as-user.sh "$username" "apps/docker/utils/lazydocker-local.sh"
fi

#todo: configure non-shared ~/_data folder for work environment with ~/.bashrc and dolphin defaults and ~/.0_data simlink for mc

# exit
echo "================================================================================"
echo ""
echo " Non-GUI related configuration of user '$username' is finished."
echo ""
echo " [!!!] All file/folder permissions changes will be available after re-login."
echo ""
echo " Please, finish GUI-related configuration by login as '$username' via GUI and run:"
echo ""
echo "   ~/initial-config.sh"
echo ""
echo "================================================================================"
