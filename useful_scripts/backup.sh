#!/bin/bash
cd "$(dirname "$0")"

mkdir -p ./backup/vdshb

rsync --archive \
      --delete \
      --progress \
      --human-readable \
      --exclude node_modules \
      /home/vdshb/_data \
      /home/vdshb/Downloads \
      /home/vdshb/.ssh \
      /home/vdshb/.gnupg \
      /home/vdshb/.config \
      /home/vdshb/.IntelliJIdea2019.3 \
      "/home/vdshb/VirtualBox VMs" \
      ./backup/vdshb \
      | tee ./backup_vdshb.log

mkdir -p ./backup/vdsheee

rsync --archive \
      --delete \
      --progress \
      --human-readable \
      --exclude node_modules \
      /home/vdsheee/_data \
      /home/vdsheee/.ssh \
      /home/vdsheee/.gnupg \
      /home/vdsheee/.config \
      /home/vdsheee/.IntelliJIdea2019.3 \
      "/home/vdshb/VirtualBox VMs" \
      ./backup/vdsheee \
      | tee ./backup_vdsheee.log